package com.reksa.karang.footballmatchschedule.model

import java.util.*

data class ExTeam1(val name: String, val score: String, val date: String)