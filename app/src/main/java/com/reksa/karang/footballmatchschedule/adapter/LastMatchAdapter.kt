package com.reksa.karang.footballmatchschedule.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.reksa.karang.footballmatchschedule.R
import com.reksa.karang.footballmatchschedule.model.ExTeam1
import kotlinx.android.synthetic.main.row_last_match.view.*

class LastMatchAdapter(val list: List<ExTeam1>): RecyclerView.Adapter<LastMatchViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LastMatchViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val view = layoutInflater.inflate(R.layout.row_last_match, parent, false)

        return LastMatchViewHolder(view)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: LastMatchViewHolder, position: Int) {
        holder.view.tv_team1.text = list.get(position).name
        holder.view.tv_score1.text = list.get(position).score

        holder.view.tv_date.text = list.get(position).date.toString()
    }
}

class LastMatchViewHolder(val view: View): RecyclerView.ViewHolder(view) {

}