package com.reksa.karang.footballmatchschedule.fragment


import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.reksa.karang.footballmatchschedule.R
import com.reksa.karang.footballmatchschedule.adapter.LastMatchAdapter
import com.reksa.karang.footballmatchschedule.model.ExTeam1
import kotlinx.android.synthetic.main.fragment_last_match.view.*
import java.text.SimpleDateFormat
import java.util.*


/**
 * A simple [Fragment] subclass.
 *
 */
class LastMatchFragment : Fragment() {

    private val list = ArrayList<ExTeam1>()


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val sdf = SimpleDateFormat("EEE, d MMM yyyy")
        val currentDate = sdf.format(Date())

        list.add(ExTeam1("arsenal", "9", currentDate))
        list.add(ExTeam1("arsenal", "9", currentDate))
        list.add(ExTeam1("arsenal", "9", currentDate))
        list.add(ExTeam1("arsenal", "9", currentDate))
        list.add(ExTeam1("arsenal", "9", currentDate))
        list.add(ExTeam1("arsenal", "9", currentDate))
        list.add(ExTeam1("arsenal", "9", currentDate))
        list.add(ExTeam1("arsenal", "9", currentDate))
        list.add(ExTeam1("arsenal", "9", currentDate))
        list.add(ExTeam1("arsenal", "9", currentDate))
        list.add(ExTeam1("arsenal", "9", currentDate))
        list.add(ExTeam1("arsenal", "9", currentDate))

        val roowView = inflater.inflate(R.layout.fragment_last_match, container, false)
        roowView.rv_last_match.layoutManager = LinearLayoutManager(activity)
        roowView.rv_last_match.adapter = LastMatchAdapter(list)



        return roowView
    }


}
